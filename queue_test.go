package queue

import "testing"

func TestNew(t *testing.T) {
	queue := New()
	if queue.Count() != 0 {
		t.Fatal("queue count == zero")
	}
}

func TestIsEmpty(t *testing.T) {
	queue := New()
	if !queue.isEmpty() {
		t.Fatal("queue is not empty after creation")
	}
}

func TestCount(t *testing.T) {
	queue := New()
	if queue.Count() != 0 {
		t.Fatal("queue count == zero")
	}
}

func TestEnqueue(t *testing.T) {
	queue := New()
	queue.Enqueue("test")
	if queue.Count() == 0 {
		t.Fatal("queue count != 0")
	}
	if queue.Count() != 1 {
		t.Fatal("queue count == 1")
	}
}

func TestDequeue(t *testing.T) {
	queue := New()
	data := queue.Dequeue()
	if data != nil {
		t.Fatal("queue item == nil")
	}

	queue.Enqueue("item")
	data = queue.Dequeue()
	if data == nil {
		t.Fatal("queue item != nil")
	}
	if queue.Count() != 0 {
		t.Fatal("queue count == zero")
	}

	queue.Enqueue("item 2")
	queue.Enqueue("item 3")
	data = queue.Dequeue()
	if data == nil {
		t.Fatal("queue item != nil")
	}
	if queue.Count() != 1 {
		t.Fatal("queue count == 1")
	}
}

func TestPeek(t *testing.T) {
	queue := New()
	data := queue.Peek()
	if data != nil {
		t.Fatal("queue item == nil")
	}

	queue.Enqueue("item")
	data = queue.Peek()
	if data == nil {
		t.Fatal("queue item != nil")
	}
	if queue.Count() != 1 {
		t.Fatal("queue count == 1")
	}
}
