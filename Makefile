PACKAGES = $(shell go list ./...)

# Build & Install

install:
	go install -v $(PACKAGES)


# Testing

.PHONY: test
test:
	go test -v $(PACKAGES)

.PHONY: cover-profile
cover-profile:
	echo "mode: count" > coverage-all.out
	$(foreach pkg,$(PACKAGES),\
		go test -coverprofile=coverage.out -covermode=count $(pkg);\
		tail -n +2 coverage.out >> coverage-all.out;)
	rm -rf coverage.out

.PHONY: cover
cover: cover-profile
	go tool cover -func=coverage-all.out

.PHONY: cover-html
cover-html: cover-profile
	go tool cover -html=coverage-all.out


# Lint

lint:
	gometalinter --tests ./... --disable=gas