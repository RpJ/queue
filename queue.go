package queue

import "sync"

// Queue struct data type.
type Queue struct {
	// values holds the queue values as an interface array so, we can add any
	// kind of item into the list.
	values []interface{}

	// count holds the queue length inside the Queue struct
	count int

	// mutex satisfy the synchronization for queue usage in goroutines
	mu sync.RWMutex
}

// New creates a queue instance.
func New() *Queue {
	return &Queue{
		values: make([]interface{}, 0),
	}
}

// Count return the length of the queue.
func (q *Queue) Count() int {
	return q.count
}

// isEmpty checks the queue if it has any item in it.
func (q *Queue) isEmpty() bool {
	if q.count == 0 {
		return true
	}

	return false
}

// Enqueue adds an item to the list(queue), then increses count of the queue.
func (q *Queue) Enqueue(value interface{}) {
	q.mu.Lock()
	defer q.mu.Unlock()

	q.values = append(q.values, value)
	q.count++
}

// Dequeue gets the first added item from the list, also descreases the count of
// queue if count > 0.
func (q *Queue) Dequeue() interface{} {
	q.mu.Lock()
	defer q.mu.Unlock()

	if q.isEmpty() {
		return nil
	}

	value := q.values[0]
	if q.count == 1 {
		q.values = q.values[:0]
	} else {
		q.values = q.values[1 : len(q.values)-1]
	}
	q.count--

	return value
}

// Peek gets the element at the front of the queue without removing it.
func (q *Queue) Peek() interface{} {
	q.mu.Lock()
	defer q.mu.Unlock()

	if q.isEmpty() {
		return nil
	}

	value := q.values[0]
	return value
}
