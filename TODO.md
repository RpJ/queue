# TODO

The present soure code implements a basic concurrent safe queue implementation. 

In order to add more features to this exercise the following steps would be:

**Networking support** 

Allowing remote usage of this queue should be a mandatory feature. I would start with a REST interface on top of it serializing the values to queue and dequeue in for example JSON.

A GRPC interface would be a next step allowing faster and typed communicatin between the clients and the server.

**Persistence support**

This queue implementation do not support persistence. A simple persistence layer could be implemented for example using an embebable database like [lebeldb](https://github.com/google/leveldb). Another possiblity could be [Redis](https://redis.io/).

Both solutions mimifies the array used to store all elements as they are key valued data stores.

**Clustering support**

Finally a clustering support would be a great feature for a distributed queue solution. But this is by far a more complex feature to implement.

I would start using a library like [Hashicorp's memberlist](https://github.com/hashicorp/memberlist) to implement a gossip protocol over all members of the cluster and then update the necessary changes on the persistence and network layers.
 